package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;
    public static void main(String[] args) {
        pokemon1 = new Pokemon("Pikachu", 94, 12);  // Eksempelnavn og HP-verdi
        pokemon2 = new Pokemon("Odish", 100,3);
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated
                // Kjempe til en er bekjempet
        System.out.println(pokemon1.toString());
        System.out.println(pokemon2.toString());
        System.out.println();

        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            // La dem kjempe
            if (pokemon1.isAlive() && pokemon2.isAlive()) {
                pokemon1.attack(pokemon2);
            }
            if (pokemon2.isAlive() && pokemon1.isAlive()) {
                pokemon2.attack(pokemon1);
            }
            //pokemon1.attack(pokemon2);
            //pokemon2.attack(pokemon1);
        }
    }
}
