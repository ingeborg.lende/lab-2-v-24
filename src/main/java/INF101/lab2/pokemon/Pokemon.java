package INF101.lab2.pokemon;

import java.util.Random;

public class Pokemon {
    ////// Oppgave 1a
    // Create field variables here:
    private String name; //Navnet til Pokemonen
    private int healthPoints; //Health points er hvor mye liv Pokémon'en har
    private int maxHealthPoints; //Hvor mange health points Pokémon'en kan maksimalt ha (og starter med)
    private int strength; //Hvor sterk Pokemonen er

    //pikachu = Pokemon()
    //lowtad = Pokemon ()
    //charmander = Pokemon()

    ///// Oppgave 1b
    // Create a constructor here:
    public Pokemon(String name,int healthPoints, int strength){
        this.name= name;
        this.healthPoints=healthPoints;
        this.maxHealthPoints=healthPoints;
        this.strength= strength;
    }

    ///// Oppgave 2
	/**
     * Get name of the pokémon
     * @return name of pokémon
     */
    public String getName() {
        return name;
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    /**
     * Get strength of the pokémon
     * @return strength of pokémon
     */
    public int getStrength() {
        return strength;
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    /**
     * Get current health points of pokémon
     * @return current HP of pokémon
     */
    public int getCurrentHP() {
        return healthPoints;
        //throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    /**
     * Get maximum health points of pokémon
     * @return max HP of pokémon
     */
    public int getMaxHP() {
        return maxHealthPoints;
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    /**
     * Check if the pokémon is alive. 
     * A pokemon is alive if current HP is higher than 0
     * @return true if current HP > 0, false if not
     */
    public boolean isAlive() {
        if (healthPoints>0) {
            return true;
        }
        if (healthPoints==0) {
            return false;  
        }
        else{
            return false;
        }
        //throw new UnsupportedOperationException("Not implemented yet.");
    }


    
    ///// Oppgave 4
    /**
     * Damage the pokémon. This method reduces the number of
     * health points the pokémon has by <code>damageTaken</code>.
     * If <code>damageTaken</code> is higher than the number of current
     * health points then set current HP to 0.
     *
     * It should not be possible to deal negative damage, i.e. increase the number of health points.
     *
     * The method should print how much HP the pokemon is left with.
     *
     * @param damageTaken
     */
    void damage(int damageTaken) {
        if (damageTaken > 0) { // Sjekk om skaden er positiv
            if (damageTaken >= healthPoints) {
                healthPoints = 0;
            } else {
                healthPoints -= damageTaken;
            }
    
            System.out.println(name + " takes " + damageTaken + " damage and is left with " + healthPoints + "/" + maxHealthPoints + " HP");
        } else {
            System.out.println("Invalid damage value. Damage must be a positive integer.");
        }
    }
        
        
        
        
        //if (damageTaken>=healthPoints) {
          //  healthPoints=0;
        //}
        //else{
          //  healthPoints=healthPoints-damageTaken;
            
        //}
        //if (healthPoints < 0) {
          //  healthPoints = 0; // Hindrer at healthPoints blir negative
        //}
        //System.out.println(name+" takes "+damageTaken+" damage and is left with "+healthPoints+"/"+maxHealthPoints+" HP");

        //throw new UnsupportedOperationException("Not implemented yet.");

    ///// Oppgave 5
    /**
     * Attack another pokémon. The method conducts an attack by <code>this</code>
     * on <code>target</code>. Calculate the damage using the pokémons strength
     * and a random element. Reduce <code>target</code>s health.
     * 
     * If <code>target</code> has 0 HP then print that it was defeated.
     * 
     * @param target pokémon that is being attacked
     */
    void attack(Pokemon target) {
        Random rand = new Random();
        int damageInflincted = (int) (rand.nextInt(this.strength+1));
        target.damage(damageInflincted);
        System.out.println(this.name+ " attacks "+ target.name );
        if (target.healthPoints<=0) {
            System.out.println(target.name+ " is defeated by "+this.name);
            
        }

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    ///// Oppgave 3
    @Override
    public String toString() {
        return name+" HP: "+"("+healthPoints+"/"+maxHealthPoints+")"+" STR: "+strength; 
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

}
